<?php 


include_once("../../../../../vendor/autoload.php");

use app\basis\bitm\seip\students\students;


 $member = new students();

 $someones = $member->index($_GET);
 $someones = $member->setData($_GET);
 

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Database</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	
<div class="data-tabel">
	<table class="table table-striped">
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Title</th>
	        <th>Action</th>
	      </tr>
	    </thead>
	    <tbody>
	      <?php $i = 0; ?>
	      <?php foreach($someones as $someone): ?> 
	      <?php echo "<tr>" .
	      	"<td>" . $i . "</td>" .
	      	"<td>" . $someone['name'] . "</td>" .
	      	
	      	"<td>" . '<a class="btn btn-primary" href="#">Single</a> <a class="btn btn-success" href="show.php?id='.$someone['id'].'">View</a> <a class="btn btn-danger" href="#">Delete</a>'  . "</td>" .
	      	
	      "</tr>"; 
	      $i++

	      ?>

	      <?php endforeach; ?>



	    </tbody>
		
  </table>
</div>



</body>
</html>