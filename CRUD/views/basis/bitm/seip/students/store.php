<?php 

	include_once("../../../../../vendor/autoload.php");

	use app\basis\bitm\seip\students\students;

	session_start();

	$obj = new students();

	if (!empty($_POST['title'])) {
		if (preg_match("/([a-z])/", $_POST['title'])) {

			$_POST['title'] = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
			$obj->setData($_POST)->store();
		} else{
			$_SESSION['massage'] = "Invalid Input";
			header('location:index.php');
		}
		
	} else{

		$_SESSION['massage'] = "Input can't be empty";
		header('location:index.php');
	}
	