<?php 

	
	namespace app\bitm\teacher;

	use PDO;

	class teacher{

		public $username = "";
		public $email = "";
		public $password = "";
		public $gender = "";
		public $id = "";

		public function setData($info = ""){
			if (array_key_exists("username", $info)) {
				$this->username = $info['username'];
			}
			if (array_key_exists("pass", $info)) {
				$this->password = $info['pass'];
			}

			if (array_key_exists("email", $info)) {
				$this->email = $info['email'];
			}
			if (array_key_exists("gender", $info)) {
				$this->gender = $info['gender'];
			}
			if (array_key_exists("id", $info)) {
				$this->id = $info['id'];
			}
			
			return $this;


		}

		 public function index()
		    {

		        $pdo = new PDO('mysql:host=localhost;dbname=teacher', 'root', '');
		        $sql = "SELECT * FROM `teacher`  ";
		        $stmt = $pdo->prepare($sql);
		        $stmt->execute();
		        $data = $stmt->fetchAll();


		        return $data;
		    }
		 public function show()
		    {

		        $pdo = new PDO('mysql:host=localhost;dbname=teacher', 'root', '');
		        $sql = "SELECT * FROM `teacher` where id=$this->id";
		        $stmt = $pdo->prepare($sql);
		        $stmt->execute();
		        $data = $stmt->fetch();


		        return $data;
		    }

		    public function delete()
		    {

		        $pdo = new PDO('mysql:host=localhost;dbname=teacher', 'root', '');
		        $sql = "DELETE FROM `teacher` WHERE `teacher`.`id` = $this->id";
		        $stmt = $pdo->prepare($sql);
		        $stmt->execute();
				if ($stmt) {
					session_start();
	            	$_SESSION['massage'] = "Successfully Deleted from Database";

					header("location:index.php");
				}
				return $data;
		    }



		public function store(){
			 try {
	            $pdo = new PDO('mysql:host=localhost;dbname=teacher', 'root', '');
	            $query = "INSERT INTO `teacher` (`id`, `username`, `password`, `email`, `gender`) VALUES (:a, :b, :c, :d, :e)";
	            $stmt = $pdo->prepare($query);
	            $stmt->execute(
	                array(
	                    ':a' => null,
	                    ':b' => $this->username,
	                    ':c' => $this->password,
	                    ':d' => $this->email,
	                    ':e' => $this->gender,
	                )
	            );

	           if ($stmt) {
	            	session_start();
	            	$_SESSION['massage'] = "Successfully saved in database";

	            	header('location:create.php');
	            } 
	        } catch
	        (PDOException $e) {
	            echo 'Error: ' . $e->getMessage();
	        }
		}
		
		public function update(){
			 try {
	            $pdo = new PDO('mysql:host=localhost;dbname=teacher', 'root', '');
	            $query = "UPDATE `teacher` SET `username` = :a, `password` = ':b', `email` = ':c', `gender` = ':d' WHERE `teacher`.`id` = $this->id";
	            $stmt = $pdo->prepare($query);
	            $stmt->execute(
	                array(
	                    ':a' => null,
	                    ':b' => $this->username,
	                    ':c' => $this->password,
	                    ':d' => $this->email,
	                    ':e' => $this->gender,
	                )
	            );


	        } catch
	        (PDOException $e) {
	            echo 'Error: ' . $e->getMessage();
	        }
		}



	}
	

