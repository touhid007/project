<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP and MySQL</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
 <div class="contact-form">
 <a class="btn btn-info" href="index.php">View List</a>
 	<form action="store.php" method="POST">
 		<div class="form-group">
			<label for="username">Username:</label>
	 		<input type="text" name="username" id="username">
 		</div>
 		<div class="form-group">
 			<label for="pass">Password:</label>
 			<input type="password" name="pass" id="pass">
 		</div>
		<div class="form-group">
			<label for="email">E-mail:</label>
 			<input type="email" name="email" id="email">
		</div>
		<div class="form-group">
			<label for="gender">Gender:</label>
 			<input type="text" name="gender" id="gender">
		</div>
		<input class="btn btn-success" type="submit" value="Send">
 	</form>

 	<p><?php if (isset($_SESSION['massage'])) {
 		echo $_SESSION['massage'];
 		unset($_SESSION['massage']);
 	} ?></p>
 </div>
	
</body>
</html>