<?php 

	include("../../../vendor/autoload.php");

	use app\bitm\teacher\teacher;

	session_start();


	$data = new teacher();
	$allData = $data->setData($_POST)->index();
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>List of Teacher</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	
 <section class="main-area">
 	<div class="main">
 		<p><?php if (isset($_SESSION['massage'])) {
	 		echo $_SESSION['massage'];
	 		unset($_SESSION['massage']);
	 	} ?></p>

	 	<a class="btn btn-info" href="create.php">Create</a>
 		<table border="1" cellpadding="5" cellspacing="5">
			<tr>
			    <th>ID</th>
			    <th>Username</th>
			    <th>Gender</th>
			    <th>E-mail</th>
			    <th>Action</th>
			</tr>

			    <?php
			    $id=1;
			    foreach ($allData as $item) : ?>
			        <tr>
			            <td> <?php echo $id++; ?></td>
			            <td><?php echo $item['username']; ?></td>
			            <td><?php echo $item['gender']; ?></td>
			            <td><?php echo $item['email']; ?></td>
			            <td>
			            	<a href="show.php?id=<?php echo $item['id'];?>">View</a>
			                <a href="update.php?id=<?php echo $item['id'];?>">Update</a>
			                <a href="delete.php?id=<?php echo $item['id']; ?>">Delete</a>


			            </td>
			        </tr>
			        <?php endforeach; ?>

			</table>
 	</div>
 </section>
	
</body>
</html>