<?php
include("../../../vendor/autoload.php");

use app\bitm\teacher\teacher;

$obj = new teacher();
$item = $obj->setData($_GET)->show();

?>

<html>
<head>
    <title>Teacher Details</title>
</head>
<body>

<a href="index.php">Back to Book List</a>

<div style="text-align: center;" class="single-person">
    <h1>Hello, My Name is <?php echo $item['username']; ?></h1>
    <p>Gender: <?php echo $item['gender']; ?></p>
    <p>E-mail: <?php echo $item['email']; ?></p>
</div>

</body>
</html>
