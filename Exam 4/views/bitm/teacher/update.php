<?php
include("../../../vendor/autoload.php");

use app\bitm\teacher\teacher;

session_start();
$update = new teacher();
$change = $update->setData($_GET)->update();

print_r($change);



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Update Your Information</title>
</head>
<body>
 <div class="contact-form">
 	<h1 style="margin:0; background: #000;color:#FFF;padding: 15px 0;">You can Update your information from here</h1>
 	<form action="change.php" method="POST">
 		<div class="form-group">
			<label for="username">Username:</label>
	 		<input type="text" name="username" id="username" value="<?php echo $change['username'];?>">
 		</div>
 		<div class="form-group">
 			<label for="pass">Password:</label>
 			<input type="password" name="pass" id="pass" value="<?php echo $change['password'];?>">
 		</div>
		<div class="form-group">
			<label for="email">E-mail:</label>
 			<input type="email" name="email" id="email" value="<?php echo $change['email'];?>">
		</div>
		<div class="form-group">
			<label for="gender">Gender:</label>
 			<input type="text" name="gender" id="gender" value="<?php echo $change['gender'];?>">
		</div>
		<input type="submit" name="submit" value="Send">
 	</form>

 	<p><?php if (isset($_SESSION['massage'])) {
 		echo $_SESSION['massage'];
 		unset($_SESSION['massage']);
 	} ?></p>
 </div>
	
</body>
</html>