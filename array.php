
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
		

		<h1 class="text-center" style="background: #000; color: #fff; padding:10px">PHP Programming</h1>
		<h2 style="padding-bottom: 50px;" class="text-center">Array Function</h2>

		

		<div class="operator">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="operator">
							<div style="width:600px;margin: auto" class="math-operator">

								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Chunk</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden"><code>
										$car = array('BMW', "Honda", "Marcedes", "Toyota", "Volvo"); <br/>
										
											print_r(array_chunk($car, 2));
									
									</code></p>

									<p><?php 
										$car = array('BMW', "Honda", "Marcedes", "Toyota", "Volvo");

										echo "<pre>";
											print_r(array_chunk($car, 2));
										echo "</pre>";
									 ?></p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Combine</h3>
									
									
									<p>
								

										$fname = array("ami", "tumi", "peter", "kaw ak jon"); <br/>
										$age   = array("25", "30", "35", "40"); <br/>

										$com = array_combine($fname, $age); <br/>

											print_r($com);
											
										
									</p>
									<p>
										<?php 
											$fname = array("ami", "tumi", "peter", "kaw ak jon");
											$age   = array("25", "30", "35", "40");

											$com = array_combine($fname, $age);

											echo "<pre>";
												print_r($com);
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Fill</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
										$fill = array_fill(1, 3, "blue"); <br/>
											
											print_r($fill); <br/>
											
									</p>

									<p>
										<?php 
											$fill = array_fill(1, 3, "blue");
											
											echo "<pre>";
												print_r($fill);
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Flip</h3>
									
									
									<p>
										$ami = array('1' => 'ami', '2'	=> 'tumi', '3'	=> "kaw na"); <br/>
										print_r(array_flip($ami));
									</p>
									
									<p>
										<?php 
											$ami = array('1' => 'ami', '2'	=> 'tumi', '3'	=> "kaw na");

											echo "<pre>";
												print_r(array_flip($ami));
											echo "</pre>";
										 ?>	
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Count Values</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
										$count_value = array('ami', 'tumi', 'tumi', 'ami', 'kaw na'); <br/>
										print_r(array_count_values($count_value));
											
									</p>

									<p>
										<?php 
											$count_value = array('ami', 'tumi', 'tumi', 'ami', 'kaw na');

											echo "<pre>";
												print_r(array_count_values($count_value));
											echo "</pre>";	
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Key Exists</h3>
									
									
									<p>
										
										$key_exist = array('Yamaha'	=> 'R15',	'Hero'	=> 'CBZ Xtrime'); <br/>

											if (array_key_exists("Yamaha", $key_exist)) { <br/>
												echo "We find this key"; <br/>
											}else{ 
												echo "There is no key in this name"; <br/>
											}

									</p>
										
									<p>
										<p>OutPut:</p>
										<?php 
											$key_exist = array('Yamaha'	=> 'R15',	'Hero'	=> 'CBZ Xtrime');

											if (array_key_exists("Yamaha", $key_exist)) {
												echo "We find this key";
											}else{
												echo "There is no key in this name";
											}
										 ?>	
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Keys</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
									
									$array_key = array("Toyota" => "Land Cruser",	"Ferari" => "New Version", "something"	=> 'Rav4'); <br/>

										
									print_r(array_keys($array_key));
										
											
									</p>

									<p>
										<?php 

											$array_key = array("Toyota" => "Land Cruser",	"Ferari" => "New Version", "something"	=> 'Rav4');

											echo "<pre>";
												print_r(array_keys($array_key));
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Merge</h3>
									
									
									<p>
										$mg1 = array('BITM', 'BASIS'); <br/>
										$mg2 = array('PHP', 'ASP.NET'); <br/>

										print_r(array_merge($mg1, $mg2));
											
									</p>
										
									<p>
										<?php 
											$mg1 = array('BITM', 'BASIS');
											$mg2 = array('PHP', 'ASP.NET');

											echo "<pre>";
												print_r(array_merge($mg1, $mg2));
											echo "</pre>";	
										 ?>
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Pad</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
									
									$pad = array('car', 'vehicle', 'bus'); <br/>

									print_r(array_pad($pad, 5, 'truck'));
								
										
											
									</p>

									<p>
										<?php 

											$pad = array('car', 'vehicle', 'bus');

											echo "<pre>";
												print_r(array_pad($pad, 5, 'truck'));
											echo "</pre>";	
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Pop</h3>
									
									<p>Array pop delete the last element of an array</p>
									
									<p>
										$pop = array('ami', 'tumi', 'tomra');<br/>

											array_pop($pop);<br/>

											print_r($pop);<br/>
										
									</p>
										
									<p>
										<?php 

											$pop = array('ami', 'tumi', 'tomra');

											array_pop($pop);

											echo "<pre>";
												print_r($pop);
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Push</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
									
										$push = array('ami', 'tumi'); <br/>

											array_push($push, 'tomra', 'amra'); <br/>

											print_r($push); <br/>
										

									</p>

									<p>
										<?php 

											$push = array('ami', 'tumi');

											array_push($push, 'tomra', 'amra');

											echo "<pre>";
												print_r($push);
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Rand</h3>
									
								
									
									<p>
										
										$rand = array('Blue', 'Black', 'Green', 'Sky');

											$rand_keys = array_rand($rand, 3);

											echo $rand[$rand_keys[0]]; <br/>
											echo $rand[$rand_keys[1]]; <br/>
											echo $rand[$rand_keys[2]]; <br/>

											print_r($rand_keys); 
									

									</p>
									<br/>
									<p>OutPut: </p>	
									<p>
										<?php 
											$rand = array('Blue', 'Black', 'Green', 'Sky');

											$rand_keys = array_rand($rand, 3);

											echo $rand[$rand_keys[0]] . "<br/>"; 
											echo $rand[$rand_keys[1]] . "<br/>";
											echo $rand[$rand_keys[2]] . "<br/>";

											echo "<pre>";
												print_r($rand_keys);
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Reverse</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
									
										$rev = array('1' => 'ami', '2' => 'tumi', '3' => 'tomra'); <br/>

											print_r(array_reverse($rev)); <br/>
										
										

									</p>

									<p>
										<?php 

											$rev = array('1' => 'ami', '2' => 'tumi', '3' => 'tomra');

											echo "<pre>";
												print_r(array_reverse($rev));
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Replace</h3>
									
								
									
									<p>
										$arr 	= array('blue', 'black', 'red'); <br/>
											$arr2	= array('brown', 'sky'); <br/>

											print_r(array_replace( $arr, $arr2 )); <br/>
										
										
									</p>
										
									<p>
										<?php 
											$arr 	= array('blue', 'black', 'red');
											$arr2	= array('brown', 'sky');

											echo "<pre>";
												print_r(array_replace( $arr, $arr2 ));
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array shift</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
									
									$shift = array('one' => 'BackDoor', 'two' => 'BruteForce', 'three' => 'hacked'); <br/>

											echo array_shift($shift); <br/>

											print_r($shift); <br/>
											
										
										
									</p>
									
									<p>The array_shift() function removes the first element from an array, and returns the value of the removed element.</p>
									<p>
										<?php 

											$shift = array('one' => 'BackDoor', 'two' => 'BruteForce', 'three' => 'hacked');

											echo array_shift($shift);

											echo "<pre>";
												print_r($shift);
											echo "</pre>";
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Slice</h3>
									
								
									
									<p>
										$slice = array('Pepsi', 'Uro Cola', 'sprite', '7up'); <br/>

										print_r(array_slice($slice, 2));
								
										
									</p>
										
									<p>
										<?php 

										$slice = array('Pepsi', 'Uro Cola', 'sprite', '7up');

										echo "<pre>";
											print_r(array_slice($slice, 2));
										echo "</pre>";

										 ?>
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">Array Splice</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
										
										$splice = array('a' => 'ami', 'b' => 'tumi', 'c' => 'tomra', 'd' => 'amra'); <br/>
											$splice1 = array('a' => 'gari', 'b' => 'ghora', 'c' => 'palki');<br/>

											array_splice($splice, 0, 2, $splice1);<br/>

											print_r($splice);

										
									</p>
									
									
									<p>
										<?php 

											$splice = array('a' => 'ami', 'b' => 'tumi', 'c' => 'tomra', 'd' => 'amra');
											$splice1 = array('a' => 'gari', 'b' => 'ghora', 'c' => 'palki');

											array_splice($splice, 0, 2, $splice1);


											echo "<pre>";
												print_r($splice);
											echo "</pre>";

										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Compact</h3>
										
										
								
									
									<p>
										$firstName = "Peter"; <br/>
											$lastName  = "Pol"; <br/>
											$age 	   = 29; <br/>

											$detail = compact('firstName', 'lastName', 'age'); <br/>

												print_r($detail); <br/>
										
									</p>
										
									<p>
										<?php 
											$firstName = "Peter";
											$lastName  = "Pol";
											$age 	   = 29;	

											$detail = compact('firstName', 'lastName', 'age');

											echo "<pre>";
												print_r($detail);
											echo "</pre>";
										 ?>

									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">In Array</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
										
										$something = array("ami", "Joe", "Glenn", "na"); <br/>

											if (in_array("Glenn", $people)) <br/>
											  { <br/>
											  echo "Match found"; <br/>
											  } <br/>
											else <br/>
											  { <br/>
											  echo "Match not found"; <br/>
											  } <br/>

										
									</p>
									
									<h3>OutPut:::</h3>
									<p>
										<?php 

											$something = array("ami", "Joe", "Glenn", "na");

											if (in_array("Glenn", $something))
											  {
											  echo "Match found";
											  }
											else
											  {
											  echo "Match not found";
											  }
										 ?>
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Search</h3>
										
										
									
									<p>
										
										
									</p>
										
									<p>
										<?php
											$a=array("a"=>"red","b"=>"green","c"=>"blue");
											echo array_search("red",$a);
										?>
									</p>
								</div> <!-- odd -->
								
								<div class="even">
									<h3 style="background: #82BD52;color: #FFF; font-weight: bold;padding:10px 0;" class="text-center">In Array</h3>
									<br/>
									<p style="background: #FFF; overflow: hidden">
										
										
										
									</p>
									
								
									<p>
										
									</p>
								</div> <!-- even -->


								<div class="odd">
									<h3 style="background: #000; color:#FFF;padding:10px 0; font-weight: bold;" class="text-center">Array Search</h3>
										
										
									
									<p>
										
										
									</p>
										
									<p>
										
									</p>
								</div> <!-- odd -->
								
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
</body>
</html>

