<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcf1687d8c8fb6366280a3bdae227ffff
{
    public static $prefixLengthsPsr4 = array (
        'a' => 
        array (
            'app\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'app\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitcf1687d8c8fb6366280a3bdae227ffff::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitcf1687d8c8fb6366280a3bdae227ffff::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
