
 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Print_R</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
		
		<h1 class="text-center">PHP Programming</h1>
		<h2 class="text-center">print_r()</h2>
		<br>
		<br>
		<br>
		<p style="text-align: center;">
			<?php 
				$var = array(
					'a' => 'Apple',
					'b' => 'Banana',
					'c' => 'Cat',
					'd' => 'Dog'
					);

				echo "<pre/>";
				print_r($var);
			 ?>
			
		</p>




</body>
</html>

