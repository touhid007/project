
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
		

		<h1 class="text-center" style="background: #000; color: #fff; padding:10px">PHP Programming</h1>
		<h2 style="padding-bottom: 50px;" class="text-center">PHP GET, POST</h2>

		

		<div class="operator">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="operator">
							<div style="width:600px;margin: auto" class="math-operator">

								<form action="store.php" method="GET">
								  <div class="form-group">
								    <label for="number">Email address:</label>
								    <input type="number" name="number" class="form-control" id="email">
								  </div>
								  <div class="form-group">
								    <label for="number1">Password:</label>
								    <input type="number" name="number1" class="form-control" id="pwd">
								  </div>
								  <select name="calculate">
								  	<option value = "addition" name = "addition"> addition (+)</option>
								  	<option value = "substraction" name="substraction">substraction (-)</option>
								  	<option value = "multiplication" name="multiplication">multiplication (x)</option>
								  	<option value = "division" name="division">Division (/)</option>
								  </select> <br/>								  
								  <button style="margin-top: 20px" type="submit" class="btn btn-default">Submit</button>
								</form>

								

							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
</body>
</html>

