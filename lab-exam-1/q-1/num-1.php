
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
		

		<h1 class="text-center" style="background: #000; color: #fff; padding:10px;text-align: center;">PHP Programming</h1>
		<h2 style="text-align: center;" class="text-center">PHP Operator</h2>

		

		<div class="operator">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div style="text-align:center;" class="operator">
							<div style="text-align: center;" class="math-operator text-center">
								
								<h3 style="background: #000; color: #FFF;text-align: center;" class="text-center">is array</h3>
								
								<p style="background: #000; color: #FFF;padding: 5px;">Input:</p>
								<code>
									$something = array('my', 'name', 'is', 'nothing');<br/>
									if(is_array($something)):<br/>
										print_r($something);<br/>
									else: <br/>
										"This is not an array";<br/>
								</code>

								<p>
									<?php $something = array('my', 'name', 'is', 'nothing'); ?>
								</p>

								<p style="background: #000; color: #FFF; padding: 5px;">Output:</p>
								<p><?php if(is_array($something)): ?>
									
									<?php print_r($something); ?>

								<?php else: ?>
									<?php "This is not an array"; ?>
								<?php endif ?></p>
								
								
								
								<!-- is array example -->

								<p style="background: #000; color: #FFF; padding: 5px;">Input:</p>
								<code>
									$something = "something going on"; <br/>
									if (!is_array(!$something)): <br/>
										echo "this is not an array"; <br/>
									else: <br/>
										echo "This is array"; 
								</code> 


								<?php $something = "something going on"; ?>
								<p style="background: #000; color: #FFF; padding: 5px;">Output</p>
								<p>
									<?php if (!is_array(!$something)): ?>
										<?php echo "this is not an array"; ?>
									
										<?php else: ?>
											<?php echo "This is array"; ?>
									<?php endif ?>

								</p>


								
								<h3 style="background: #ddd; color: #000;text-align: center;" class="text-center">is null</h3>
								<!-- is null example -->

								<p style="background: #000; color: #FFF; padding: 5px;">Input:</p>
								<code>
									
										

								</code> 

								<p style="background: #000; color: #FFF; padding: 5px;">Output:</p>
								<p>
									<?php $nothing = 1; ?>
									<?php if (is_null($nothing)) {
										echo "it's null";
									}
									else{
											echo "it's not null";
										} ?>
									
								</p>


							


							<h3 style="background: #ddd; color: #000;text-align: center;" class="text-center">print_r</h3>

							<p style="background: #000; color: #FFF; padding: 5px;">Input:</p>
							<code>
								$ami = array('ami', 'tumi', 'kaw na'); <br>	

								print_r($ami); <br>

							</code> 
							
							<p style="background: #000; color: #FFF; padding: 5px;">Output:</p>
							<p>
								<?php $ami = array('ami', 'tumi', 'kaw na'); ?>
								<pre>
									<?php print_r($ami); ?>
								</pre>
							</p>



							<h3 style="background: #ddd; color: #000;text-align: center;" class="text-center">is bool</h3>

							<p style="background: #000; color: #FFF; padding: 5px;">Input:</p>
							<code>
								$boo = false ; <br>	

								if (is_bool($boo)): <br>
								echo "This is boolian"; <br>
								endif <br>

							</code> 
							
							<p style="background: #000; color: #FFF; padding: 5px;">Output:</p>
							<p>
								<?php $boo = false ;  ?>
								<?php if (is_bool($boo)): ?>
									<?php echo "This is boolian"; ?>
								<?php endif ?>
							</p>
							



							
							<h3 style="background: #ddd; color: #000;text-align: center;" class="text-center">is float</h3>

							<p style="background: #000; color: #FFF; padding: 5px;">Input:</p>
							<code>
								$fla = 1.205 ; <br>	

								if (is_float($fla)): <br>
								echo "This is float"; <br>
								endif <br>

							</code> 
							
							<p style="background: #000; color: #FFF; padding: 5px;">Output:</p>
							<p>
								<?php $fla = 1.205 ; ?>
								<?php if (is_float($fla)): ?>
									<?php echo "This is float"; ?>
								<?php endif ?>
							</p>
							







								







								
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
</body>
</html>

