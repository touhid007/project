
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
		

		<h1 class="text-center" style="background: #000; color: #fff; padding:10px">PHP Programming</h1>
		<h2 class="text-center">PHP Operator</h2>

		

		<div class="operator">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div style="text-align:center;" class="operator">
							<div class="math-operator">
								
								<?php 
									$a = 2;
									$b = 3;
								 ?>
								
								<h3 style="background: blue; color: #FFF; padding:20px ">Math Operator</h3>
								<p>2 + 3 = <?php echo $a + $b ?></p>
								<p>2 - 3 = <?php echo $a - $b ?></p>
								<p>2 x 3 = <?php echo $a * $b ?></p>
								<?php $b = 6; $c = 2; ?>
								<p>6/2 = <?php echo $b/$c ?></p>
								<p>6%2 = <?php echo $b%$c ?></p>
								<p>6**2 = <?php echo $b**$c ?></p>

								<h3 style="background: blue; color: #FFF; padding:20px ">PHP Assignment Operator</h3>

								<p>here $a = 20</p>
								<?php 
									$a = 20;
								?>
								<p>$a = 20 -> <?php echo $a; ?></p>
								<p>$a += 10 -> <?php $a += 10; echo $a; ?> </p>
								<p>$a -= 10 -> <?php $a -= 20; echo $a; ?> </p>
								<?php 
									$a = 20;
								?>
								<p>$a *= 20 -> <?php $a *= 4; echo $a; ?> </p>
								<?php 
									$a = 20;
								?>
								<p>$a /= 4 -> <?php $a /= 4; echo $a; ?> </p>
								<?php 
									$a = 20;
								?>
								<p>$a %= 4 -> <?php $a %= 4; echo $a; ?> </p>
								
								<h3 style="background: blue; color: #FFF; padding:20px ">PHP Comparison  Operator</h3>

								<?php 
									$a = "20"; 
									$b = 20 
								?>
								
								<p>Here $a = "20" and $b = 20</p>
								<p>$a == $b => <?php
												 if($a == $b){
												   echo "True";
												 	}
												 else{
												 		echo "false";
												 		} ?>
												 		
								</p>
								<p>$a === $b => <?php
												 if($a === $b){
												   echo "True";
												 	}
												 else{
												 		echo "false";
												 		} ?>
												 		
								</p>
								
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
</body>
</html>

