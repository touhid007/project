<?php 

	namespace app\basis\bitm\seip\students;
	use PDO;


	class students{

		public $name = "";

		public function setData($data = ""){
			$this->name = $data['title'];

			return $this;
		}

		public function store(){
		

			try {
			  $pdo = new PDO("mysql:host=localhost;dbname=management", "root", "");
			 
			  $query = "INSERT INTO `students` (`id`, `title`) VALUES (:a, :b)";
			  $stmt = $pdo->prepare($query);
			  $stmt->execute(
			  		array(
			  			':a'	=> null,
			  			':b'	=> $this->name	
			  			)
			  	);

			  if ($stmt) {
			  	session_start();
			  	$_SESSION['massage'] = "successfully submitted";

			  	header('location:index.php');
			  }
			 
			} catch(PDOException $e) {
			  echo 'Error: ' . $e->getMessage();
		}
	}


}



