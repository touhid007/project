
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
		

		<h1 class="text-center" style="background: #000; color: #fff; padding:10px">PHP Programming</h1>
		<h2 class="text-center">PHP String Function</h2>

		

		<div class="operator">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div style="text-align:center;" class="operator">
							<div class="math-operator">

								<h2 style="background:Black;color:#FFF">addcslashes()</h2>
								<p>Input: </p>
								<code>
									$str = "I don't know where I am !";<br/>

								   	echo addcslashes($str, 'w');
								</code>
								<br/>
								<br/>
								<br/>
								<p>Output: </p>
								<p>
								   <?php
								   		$str = "I don't know where I am !";

								   		echo addcslashes($str, 'w');
								    ?>
								</p>
								

								<h2 style="background:Black;color:#FFF">addslashes()</h2>
								<p>Input: </p>
								<code>
									$str = 'I don't know "where" I am !';<br/>

								   	echo addslashes($str);
								</code>
								<br/>
								<br/>
								<br/>
								<p>Output: </p>
								<p>
								   <?php
								   		$str = 'I do not know "where" I am !';

								   		echo addslashes($str);
								    ?>
								</p>
								


								

								<h2 style="background:Black;color:#FFF">chop()</h2>
								<p>Input: </p>
								<code>
									$str = "I don't know where I am !"; <br/>

								   	echo chop($str, '!');
								</code>
								<br/>
								<br/>
								<br/>
								<p>Output: </p>
								<p>
								   <?php
								   		$str = "I don't know where I am !";

								   		echo chop($str, '!');
								    ?>
								</p>


								<h2 style="background:Black;color:#FFF">chunk_split()</h2>
								<p>Input: </p>
								<code>
									$str = "I don't know where I am !"; <br/>

								   	echo chunk_split($str, 4,  '...');
								</code>
								<br/>
								<br/>
								<br/>
								<p>Output: </p>
								<p>
								   <?php
								   		$str = "I don't know where I am !";

								   		echo chunk_split($str, 4,  '...');
								    ?>
								</p>


								<h2 style="background:Black;color:#FFF">convert_uuencode()</h2>
								<p>Input: </p>
								<code>
									$str = "I don't know where I am !"; <br/>

								   	$something = convert_uuencode($str); <br/>
								   	echo $something; <br/>
								</code>
								<br/>
								<br/>
								<br/>
								<p>Output: </p>
								<p>
								   <?php
								   		$str = "I don't know where I am !";

								   		$something = convert_uuencode($str);
								   		echo $something;
								    ?>
								</p>


								<h2 style="background:Black;color:#FFF">convert_uudecode()</h2>
								<p>Input: </p>
								<code>
									$str = "I don't know where I am !"; <br/>

								   		$nothing = convert_uudecode($something); <br/>
								   		echo $nothing;
								</code>
								<br/>
								<br/>
								<br/>
								<p>Output: </p>
								<p>
								   <?php
								   		$str = "I don't know where I am !";

								   		$nothing = convert_uudecode($something);
								   		echo $nothing;
								    ?>
								</p>

								
								<h2 style="background:Black;color:#FFF">str_split()</h2>
								<p>Input: </p>
								<code>
									$str = "I don't know where I am !"; <br/>

								   		$spl = str_split($str); <br/>
								   		print_r($spl);
								</code>
								<br/>
								<br/>
								<br/>
								<p>Output: </p>
								<p>
								   <?php
								   		$str = "I don't know where I am !";;

								   		$spl = str_split($str);
								   		print_r($spl);
								    ?>
								</p>

								

								

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
</body>
</html>

