<?php 

include_once("../../../../../vendor/autoload.php");

use app\basis\bitm\seip\students\students;

$obj = new students();

$details = $obj -> setData($_GET)->show();

 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Details</title>
 	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
 </head>
 <body>
 	<header class="header-area">
 		<div class="container">
 			<div class="row">
 				<div class="col-sm-12">
 					<div class="member-title">
 						<h1>Details of <?php echo $details['name']; ?></h1>
 					</div>
 					<div class="member-details">
 						<p>Institute Name: <?php echo $details['institute']; ?></p>
 						<p>Member E-mail: <?php echo $details['email']; ?></p>
 					</div>
 				</div>
 			</div>
 		</div>
 	</header>
 </body>
 </html>